# Build a Word Counting program

CC     = gcc
CFLAGS = -g -Wall
SRC = memory_scanner_jatinp.c memory_scanner.c 

TARGET = memory_scanner

all: $(TARGET)

memory_scanner: $(SRC)
	$(CC) $(CFLAGS) -o $(TARGET) $(SRC)

clean:
	rm $(TARGET)

