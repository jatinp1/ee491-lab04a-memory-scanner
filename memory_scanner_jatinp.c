#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

int main (int argc, char *argv[]) {
   // Check if there is the file name
   if (argc < 2){
      fprintf(stderr,"Usage: memory_scanner %s\n",argv[1]);
      exit(EXIT_FAILURE);
   }
   FILE *filePointer = NULL;
   //First open file to see if it exists
   filePointer = fopen(argv[1], "r");
   if (filePointer == NULL){
      fprintf(stderr,"%s: Can’t open [%s]\n",argv[0], argv[1]);
      exit(EXIT_FAILURE);
   }

   printf("Hello World");
}
